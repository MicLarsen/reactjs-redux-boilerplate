import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

import Router from './app/router/router'
import './App.css';
import logo from './logo.svg';

const Navigation = (props) => ( 
  <nav>
    <div>
      <NavLink to='/'>
        Mainpage
      </NavLink>
    </div>
  </nav>
)


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ReactJS with Redux - Boilerplate</h1>
        </header>
        <Navigation />
        <Router/>
      </div>
    );
  }
}

export default App;
