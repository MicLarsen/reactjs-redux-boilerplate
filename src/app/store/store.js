import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'
import rootReducers from '../reducers'

const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(rootReducers, applyMiddleware(middleware, thunk))

export default store